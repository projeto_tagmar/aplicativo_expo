import { Canvas, useThree } from "@react-three/fiber/native";
import React from "react";
import { View } from "react-native";
import { Vector3 } from "three";
import Ambient from "../../src/physics/Ambient";
import D20 from "../../src/physics/Dice/D20";
import Lights from "../../src/physics/Lights";
import { PhysicsProvider } from "../../src/physics/PhysicsProvider";
import PoolTable from "../../src/physics/Table/PoolTable";

// https://codesandbox.io/p/sandbox/reverent-lalande-5qdk4j?file=%2Fsrc%2Findex.js%3A1%2C1-359%2C1

const Gravitational = () => {
	const camera = useThree((state) => state.camera);

	camera.position.set(0, 0, 20);
	camera.lookAt(new Vector3(0, 0, 0));

	return (
		<PhysicsProvider>
			<D20 position={[0, 0, 9]} coluna={18} />
			<PoolTable />
		</PhysicsProvider>
	);
};

function Dados() {
	return (
		<View style={{ flex: 1 }}>
			<View style={{ flex: 1 }}>
				<Canvas shadows gl={{ alpha: true }}>
					<Ambient />
					<Lights />
					<Gravitational />
				</Canvas>
			</View>
		</View>
	);
}

export default Dados;
