import React from "react";
import { useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import Switch from "../../src/components/Switch";
import Label from "../../src/components/Label";
import Button from "../../src/components/Button";
import { ColorType } from "../../src/theme";
// import SheetCard from "../../src/components/Sheet/Card";

// const mockFicha: Ficha = {
//   _id: "asdf",
//   name: "nome",
//   classe: "rastreador",
//   narrador: "fulano",
//   nivel: 2,
//   xp: 20,
//   raca: "elfo",
//   criacao: new Date(),
// };
function Configuracoes() {
	const [on, setOn] = useState<boolean>(true);
	function switchChange(): void {
		setOn((last) => !last);
	}

	function onPress(): void {
		console.log("apertou");
	}

	return (
		<ScrollView
			contentContainerStyle={{
				alignItems: "center",
				justifyContent: "center",
			}}
		>
			<View style={style.line}>
				<Switch enabled={on} onChange={switchChange} />
				<Switch enabled={true} onChange={switchChange} />
				<Switch enabled={false} onChange={switchChange} />
			</View>
			<Label isPrimary={false} isMedium={false}>
				compact - neutral
			</Label>
			<Label isPrimary={true} isMedium={false}>
				compact - primary
			</Label>
			<Label isPrimary={false} isMedium={true}>
				medium - neutral
			</Label>
			<Label isMedium={true} isPrimary={true}>
				medium - primary
			</Label>
			<View>
				<View style={style.line}>
					<Button label="Label" onPress={onPress} isSmall={true} />
					<Button
						label="Label"
						onPress={onPress}
						isSmall={true}
						colorType={ColorType.Secondary}
					/>
					<Button
						label="Label"
						onPress={onPress}
						isSmall={true}
						colorType={ColorType.Tertiary}
					/>
					<Button
						label="Label"
						onPress={onPress}
						isSmall={true}
						disabled={true}
					/>
				</View>
				<View style={style.line}>
					<Button label="Label" onPress={onPress} />
					<Button
						label="Label"
						onPress={onPress}
						colorType={ColorType.Secondary}
					/>
					<Button
						label="Label"
						onPress={onPress}
						colorType={ColorType.Tertiary}
					/>
					<Button label="Label" onPress={onPress} disabled={true} />
				</View>
				<View>{/* <SheetCard ficha={{}} /> */}</View>
			</View>
		</ScrollView>
	);
}

const style = StyleSheet.create({
	line: {
		gap: 2,
		margin: 5,
		flexDirection: "row",
	},
});

export default Configuracoes;
