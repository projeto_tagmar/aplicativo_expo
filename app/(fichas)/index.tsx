import React from "react";
import { Link } from "expo-router";
import { ImageBackground, StyleSheet, Text, View } from "react-native";
import Button from "../../src/components/Button";

// import useFichas from "../../src/hooks/fichas/useFichas";

const some = require("../../src/assets/images/some.png");
const empty = require("../../src/assets/images/empty.png");

function Home() {
	// const { fichas } = useFichas();
	// const hasFichas = fichas.length > 0;
	return (
		<View style={style.container}>
			<ImageBackground
				// biome-ignore lint/correctness/noConstantCondition: <explanation>
				source={/*hasFichas*/ !true ? some : empty}
				style={style.image}
			>
				{true && (
					<>
						<Text style={style.name}>Fichas</Text>
						{/* {fichas.map((item) => (
              <Link key={item._id} href={{ pathname: "details", params: { name: item.name } }}>
                Nome: {item.name}
              </Link>
            ))} */}
					</>
				)}
				<View style={style.buttonContainer}>
					<Button label="Adicionar ficha" />
				</View>
			</ImageBackground>
		</View>
	);
}

const style = StyleSheet.create({
	container: {
		flex: 1,
	},
	image: {
		flex: 1,
		paddingBottom: 10,
		justifyContent: "flex-end",
	},
	name: {},
	buttonContainer: {
		marginHorizontal: "10%",
	},
});

export default Home;
