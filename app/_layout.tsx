import { Drawer } from "expo-router/drawer";
import React from "react";
import DrawerContent from "../src/components/DrawerContent";
import { drawerItems } from "../src/components/DrawerContent/drawerItems";
import { drawerOptions } from "../src/components/DrawerContent/options";
import Root from "../src/components/Root";

// global.THREE = global.THREE || THREE;

export const unstable_settings = {
	// Ensure any route can link back to `/`
	initialRouteName: "(dados)/index",
};

const MainLayout = () => {
	return (
		<Root>
			<Drawer
				drawerContent={DrawerContent}
				screenOptions={drawerOptions}
				initialRouteName={drawerItems[0].name}
			>
				{drawerItems.map((item, index) => (
					<Drawer.Screen
						key={`${index}`}
						name={item.name}
						options={{
							title: item.options.title,
						}}
					/>
				))}
			</Drawer>
		</Root>
	);
};

export default MainLayout;
