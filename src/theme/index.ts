import { DefaultTheme } from "@react-navigation/native";

const Colors = {
	fundo: {
		normal: "#121212",
		modal: "#2A2A2A",
	},
	branco: {
		_32: "rgba(255,255,255,32)",
		_42: "rgba(255,255,255,42)",
		_70: "rgba(255,255,255,70)",
		_100: "#ffffff",
	},
	cinza: {
		claro: "#717171",
		escuro: "#D0D0D0",
	},
	primaria: {
		normal: "#70B0E0",
		dark: "#052640",
		light: "#BADFFB",
	},
	secundaria: {
		normal: "#FFAC3D",
		dark: "#B06A0B",
		light: "#F3D9B5",
	},
	terciaria: {
		normal: "#496AD0",
		dark: "#172A65",
		light: "#9AADE9",
	},
	erro: "#ff7878",
} as const;

const poppins100 = "Poppins_100Thin";
const poppins400 = "Poppins_400Regular";
const poppins700 = "Poppins_700Bold";

const Fonts = {
	Poppins100: poppins100,
	Poppins400: poppins400,
	Poppins700: poppins700,
} as const;

const TagmarTheme: typeof DefaultTheme = {
	colors: {
		primary: Colors.primaria.normal,
		background: Colors.fundo.normal,
		text: Colors.branco._100,
		card: Colors.fundo.modal,
		border: Colors.secundaria.light,
		notification: Colors.fundo.modal,
	},
	dark: false,
} as const;

enum ColorType {
	Primary = 1,
	Secondary = 2,
	Tertiary = 3,
}

export { TagmarTheme, Colors, Fonts, ColorType };
