import {
	Poppins_100Thin,
	Poppins_400Regular,
	Poppins_700Bold,
	useFonts,
} from "@expo-google-fonts/poppins";
import * as SplashScreen from "expo-splash-screen";
import { useEffect } from "react";
import { useColorScheme } from "react-native";

type Out = {
	isLoaded: boolean;
};

SplashScreen.preventAutoHideAsync();

async function hideSplash() {
	await SplashScreen.hideAsync();
}

const useRootLoader = (): Out => {
	const [fontsLoaded] = useFonts({
		Poppins_100Thin,
		Poppins_700Bold,
		Poppins_400Regular,
	});

	const scheme = useColorScheme();

	console.log({ scheme });

	useEffect(() => {
		if (fontsLoaded) {
			hideSplash();
		}
	}, [fontsLoaded]);

	return {
		isLoaded: fontsLoaded,
	};
};

export default useRootLoader;
