import { ThemeProvider, DarkTheme } from "@react-navigation/native";
import React from "react";
import { PropsWithChildren } from "react";
import useRootLoader from "./useRootLoader";

const Root = ({ children }: PropsWithChildren) => {
	const { isLoaded } = useRootLoader();
	if (!isLoaded) {
		return null;
	}
	return <ThemeProvider value={DarkTheme}>{children}</ThemeProvider>;
};

export default Root;
