// import React from "react";
// import { StyleSheet, Text, View } from "react-native";

// import Button from "../../Button";
// import { MaterialIcon } from "../../Button/buttonIcons";

// interface ISheetCard {
//   ficha: Ficha;
// }
// function SheetCard({ ficha }: ISheetCard) {
//   const { criacao } = ficha;
//   const data = `${criacao.getDate()}/${criacao.getMonth() + 1}/${criacao.getFullYear()}`;

//   const ElfoDourado = Assets.getRaca(Assets.RacaAsset.ELFO_DOURADO);
//   const Rastreador = Assets.getProfissao(Assets.ProficaoAsset.RASTREADOR);
//   return (
//     <View style={style.card}>
//       <View style={style.info}>
//         <ElfoDourado />
//         <View style={style.infoTextContainer}>
//           <Text style={{ color: "white" }}>Nome: {ficha.name}</Text>
//           <Text>Criação: {data}</Text>
//           <Text>XP: {ficha.xp}</Text>
//           <Text>Nível: {ficha.nivel}</Text>
//           <Text>Narrador: {ficha.narrador}</Text>
//         </View>
//         <Rastreador />
//       </View>
//       <View style={style.actions}>
//         <Button startIcon={MaterialIcon.REMOVE_RED_EYE_OUTLINED} style={style.actionsButtons} />
//         <Button startIcon={MaterialIcon.EDIT} style={style.actionsButtons} />
//         <Button startIcon={MaterialIcon.STAR_BORDER} style={style.actionsButtons} />
//         <Button startIcon={MaterialIcon.ARCHIVE_OUTLINED} style={style.actionsButtons} />
//         <Button startIcon={MaterialIcon.DELETE} style={style.actionsButtons} />
//       </View>
//     </View>
//   );
// }

// const style = StyleSheet.create({
//   card: {
//     borderRadius: 5,
//     backgroundColor: `${Colors.fundo.normal}46`,
//     justifyContent: "space-between",
//     paddingHorizontal: 17,
//     paddingVertical: 20,
//   },
//   info: {
//     flexDirection: "row",
//     justifyContent: "space-between",
//     alignItems: "center",
//   },
//   infoTextContainer: {
//     flexDirection: "column",
//   },
//   actions: {
//     flexDirection: "row",
//     gap: 10,
//     justifyContent: "space-between",
//   },
//   actionsButtons: {
//     justifyContent: "center",
//   },
// });

// export default SheetCard;
