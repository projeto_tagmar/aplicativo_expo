import React from "react";
import { Text, View } from "react-native";
import { Drawer } from "expo-router/drawer";
import {
	DrawerContentComponentProps,
	DrawerContentScrollView,
	DrawerItemList,
} from "@react-navigation/drawer";
import UsuarioInfo from "./UsuarioInfo";

const DrawerContent = (props: DrawerContentComponentProps) => {
	return (
		<DrawerContentScrollView {...props}>
			<UsuarioInfo />
			<DrawerItemList {...props} />
		</DrawerContentScrollView>
	);
};

export default DrawerContent;
