import { DrawerNavigationOptions } from "@react-navigation/drawer";
import { Colors, Fonts } from "../../theme";

const drawerOptions: DrawerNavigationOptions = {
	headerStyle: {
		backgroundColor: Colors.fundo.normal,
	},
	headerTintColor: Colors.branco._100,
	headerShadowVisible: true,
	headerTitleStyle: {
		fontWeight: "bold",
		fontFamily: Fonts.Poppins700,
	},
	keyboardDismissMode: "on-drag",
	drawerLabelStyle: {
		fontSize: 16,
		fontFamily: Fonts.Poppins700,
	},
	drawerActiveTintColor: Colors.secundaria.normal,
	drawerActiveBackgroundColor: `${Colors.secundaria.light}4d`,
} as const;

export { drawerOptions };
