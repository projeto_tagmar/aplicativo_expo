import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Fonts } from "../../../theme";

const UsuarioInfo = () => {
	return (
		<View style={style.container}>
			<View style={style.user}>
				<Text>Imagem</Text>
				<View style={style.userContent}>
					<Text style={style.text1}>Nome</Text>
					<Text style={style.text2}>0 fichas</Text>
				</View>
			</View>
			<Text style={style.email}>email</Text>
		</View>
	);
};

const style = StyleSheet.create({
	container: {
		left: 20,
		marginTop: 10,
		marginBottom: 40,
	},
	user: {
		gap: 16,
		marginBottom: 16,
		flexDirection: "row",
	},
	userContent: {
		gap: 12,
	},
	text1: {
		fontSize: 14,
		fontFamily: Fonts.Poppins700,
	},
	text2: {
		fontSize: 14,
		fontFamily: Fonts.Poppins400,
	},
	email: {
		fontSize: 12,
		fontFamily: Fonts.Poppins400,
		opacity: 0.7,
	},
});

export default UsuarioInfo;
