type MenuItem = {
	name: string;
	options: {
		title: string;
	};
};

const drawerItems: MenuItem[] = [
	{
		name: "(fichas)/index",
		options: {
			title: "Fichas",
		},
	},
	{
		name: "(personagemAtual)/index",
		options: {
			title: "Ficha",
		},
	},
	{
		name: "(dados)/index",
		options: {
			title: "Dados",
		},
	},
	{
		name: "(magias)/index",
		options: {
			title: "Lista de Magias",
		},
	},
	{
		name: "(tecnicasDeCombate)/index",
		options: {
			title: "Lista de Técnicas de Combate",
		},
	},
	{
		name: "(mapas)/index",
		options: {
			title: "Mapas",
		},
	},
	{
		name: "(configuracoes)/index",
		options: {
			title: "Configurações",
		},
	},
	{
		name: "(sobre)/index",
		options: {
			title: "Sobre",
		},
	},
];

export { drawerItems };
