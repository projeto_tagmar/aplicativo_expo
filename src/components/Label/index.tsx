import React from "react";
import { StyleSheet, Text, TextProps, useColorScheme } from "react-native";
import { Colors, Fonts } from "../../theme";

type ILabel = {
	isMedium?: boolean;
	isPrimary?: boolean;
} & TextProps;

function Label({ isMedium = false, isPrimary = false, ...props }: ILabel) {
	const scheme = useColorScheme();

	const localStyle = style(isMedium, scheme === "dark", isPrimary);

	return <Text {...props} style={localStyle.text} />;
}

const style = (isMedium = true, isDark = true, isPrimary = true) => {
	let color = "#242a37";
	if (isDark) {
		if (isPrimary) {
			color = Colors.primaria.normal;
		} else {
			color = Colors.branco._100;
		}
	} else {
		if (isPrimary) {
			color = "#0d6cf2";
		} else {
			color = "#242a37";
		}
	}
	return StyleSheet.create({
		text: {
			fontWeight: "bold",
			fontFamily: Fonts.Poppins400,
			fontSize: isMedium ? 14 : 12,
			color,
		},
	});
};

export default Label;
