import { MaterialIcon, getIcon } from "./buttonIcons";
import React from "react";
import {
	Pressable,
	PressableProps,
	StyleSheet,
	Text,
	View,
} from "react-native";
import { ColorType, Colors, Fonts } from "../../theme";
import Material from "@expo/vector-icons/MaterialIcons";

type IButton = {
	label?: string;
	isSmall?: boolean;
	colorType?: ColorType;
	startIcon?: MaterialIcon;
} & PressableProps;

interface ButtonType {
	default: string;
	pressed: string;
}

function getColor(btType: ColorType): ButtonType {
	switch (btType) {
		case ColorType.Primary: {
			return {
				default: Colors.secundaria.normal,
				pressed: Colors.secundaria.light,
			};
		}
		case ColorType.Secondary: {
			return {
				default: Colors.primaria.normal,
				pressed: Colors.primaria.light,
			};
		}
		case ColorType.Tertiary: {
			return {
				default: Colors.fundo.normal,
				pressed: Colors.cinza.claro,
			};
		}
	}
}

function Button({
	label,
	onPress,
	disabled = false,
	isSmall = false,
	colorType = ColorType.Primary,
	startIcon,
	...props
}: IButton) {
	const localStyle = styles(isSmall, disabled ?? false, colorType);

	const icon = getIcon(startIcon);

	return (
		<View style={localStyle.buttonContainer}>
			<Pressable
				{...props}
				onPress={onPress}
				android_ripple={{
					borderless: true,
				}}
			>
				{/* biome-ignore lint/suspicious/noExplicitAny: <explanation> */}
				{startIcon && <Material name={icon as any} fill={Colors.branco._100} />}
				{label && <Text style={localStyle.buttonLabel}>{label}</Text>}
			</Pressable>
		</View>
	);
}

const styles = (isSmall: boolean, disabled: boolean, color: ColorType) =>
	StyleSheet.create({
		buttonContainer: {
			borderRadius: 2,
			paddingHorizontal: 8,
			paddingVertical: isSmall ? 4 : 5,
			backgroundColor: disabled ? Colors.cinza.claro : getColor(color).default,
		},
		buttonLabel: {
			fontSize: isSmall ? 12 : 16,
			display: "flex",
			fontWeight: "600",
			fontStyle: "normal",
			alignItems: "center",
			alignSelf: "center",
			fontFamily: Fonts.Poppins700,
			color: disabled ? Colors.cinza.escuro : Colors.branco._100,
		},
	});

export default Button;
