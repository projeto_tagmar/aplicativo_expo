import Material from "@expo/vector-icons/MaterialIcons";

enum MaterialIcon {
	REMOVE_RED_EYE_OUTLINED = "remove_red_eye_outlined",
	EDIT = "edit",
	STAR_BORDER = "star_border",
	ARCHIVE_OUTLINED = "archive_outlined",
	DELETE = "delete",
}

function getIcon(from?: MaterialIcon) {
	switch (from) {
		case MaterialIcon.REMOVE_RED_EYE_OUTLINED: {
			return Material.glyphMap["remove-red-eye"];
		}
		case MaterialIcon.ARCHIVE_OUTLINED: {
			return Material.glyphMap.archive;
		}
		case MaterialIcon.EDIT: {
			return Material.glyphMap.edit;
		}
		case MaterialIcon.STAR_BORDER: {
			return Material.glyphMap["star-outline"];
		}
		case MaterialIcon.DELETE: {
			return Material.glyphMap.delete;
		}
		default:
			return Material.glyphMap.nature;
	}
}

export { MaterialIcon, getIcon };
