import React from "react";
import { Switch as S } from "react-native";
import { Colors } from "../../theme";

interface ISwitch {
	enabled: boolean;
	onChange: (val: boolean) => void | Promise<void>;
}

function Switch({ enabled = false, onChange }: ISwitch) {
	return (
		<S
			value={enabled}
			onValueChange={onChange}
			ios_backgroundColor={style.bg}
			thumbColor={enabled ? style.thumbTrue : style.thumbFalse}
			trackColor={{ false: style.trackFalse, true: style.trackTrue }}
		/>
	);
}

const style = {
	bg: Colors.fundo.modal,
	trackTrue: Colors.secundaria.normal,
	trackFalse: Colors.cinza.claro,
	thumbTrue: Colors.branco._100,
	thumbFalse: Colors.cinza.escuro,
} as const;

export default Switch;
