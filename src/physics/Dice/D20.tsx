import * as CANNON from "cannon-es";
import React from "react";
import THREE, { Vector3 } from "three";
import { useCannon } from "../PhysicsProvider";
import { Materials, getObjectProps } from "../materials";
import { changeToColor, getFaces, getVertices, setFaceColors } from "./util";
import { useFrame, useThree } from "@react-three/fiber";
import { ResultColors, getResultFromColor } from "../../util/constants";
import * as BufferGeometryUtils from "three/examples/jsm/utils/BufferGeometryUtils";

type Props = {
	coluna: number;
	position: [number, number, number];
};

const D20 = ({ position, coluna }: Props) => {
	const diceProp = getObjectProps(Materials.METAL);

	const ref = useCannon({ mass: 0.1 }, (diceBody) => {
		const mesh: THREE.Mesh = ref.current;

		setFaceColors(mesh, coluna, diceProp);

		const threePosition = (
			mesh.geometry.attributes.position as THREE.BufferAttribute
		).array;

		const icosahedronFaces: number[][] = getFaces(threePosition);
		const icosahedronPoints: CANNON.Vec3[] = getVertices(threePosition);
		const icosahedronShape = new CANNON.ConvexPolyhedron({
			vertices: icosahedronPoints,
			faces: icosahedronFaces,
		});

		function onStop(a: { target: CANNON.Body }) {
			// const points = geom.getAttribute("position").array;
			// console.log({ points });
			// const points = mesh.geometry.getAttribute("normal").array;
			const colors = mesh.geometry.getAttribute("color").array;

			const ttt = a.target.shapes[0];

			const faces: {
				a: number;
				b: number;
				c: number;
				normal: THREE.Vector3;
			}[] = [];
			const position = mesh.geometry.getAttribute("position");

			for (let i = 0; i < position.count; i += 3) {
				const face = {
					a: i,
					b: i + 1,
					c: i + 2,
					normal: new THREE.Vector3(),
				};
				faces.push(face);
			}

			for (let j = 0; j < faces.length; j++) {
				const face = faces[j];
				const pointA = new Vector3(
					position.getX(face.a),
					position.getY(face.a),
					position.getZ(face.a),
				);
				const pointB = new Vector3(
					position.getX(face.b),
					position.getY(face.b),
					position.getZ(face.b),
				);
				const pointC = new Vector3(
					position.getX(face.c),
					position.getY(face.c),
					position.getZ(face.c),
				);

				const faceTriangule = new THREE.Triangle(pointA, pointB, pointC);

				faceTriangule.getNormal(faces[j].normal);
			}

			type T = {
				normal: number;
			};
			const sort = (a: T, b: T) => {
				return a.normal === b.normal ? 0 : b.normal - a.normal;
			};

			const [faceA, faceB] = faces
				.map((item, index) => {
					const n = item.normal;
					return {
						normal: n.z,
						index,
					};
				})
				.filter((item) => item.normal > 0)
				.sort(sort);

			const tA = faces[faceA.index];
			const tB = faces[faceB.index];

			console.log({ tA, tB });

			// const r = colors[index];
			// const g = colors[index + 1];
			// const b = colors[index + 2];
			// const color = new THREE.Color(r, g, b);
			// console.log("cor", color.getHexString());
			// const value = color.getHex();
			// console.log({ value });
			// changeToColor(mesh, value);
		}

		diceBody.addShape(icosahedronShape);
		const [x, y, z] = position;
		diceBody.position.set(x, y, z);
		diceBody.material = diceProp.material;
		diceBody.allowSleep = true;
		diceBody.sleepSpeedLimit = 0.3;
		diceBody.addEventListener("sleep", onStop);

		diceBody.velocity.setZero();
		diceBody.angularVelocity.setZero();
		const force = 0.2 * Math.random();
		diceBody.applyImpulse(
			new CANNON.Vec3(-force, force, -1),
			new CANNON.Vec3(
				0.5 * Math.random(),
				0.5 * Math.random(),
				0.5 * Math.random(),
			),
		);
	});

	// const camera = useThree((state) => state.camera);
	// useFrame(() => {
	// 	if (ref.current) {
	// 		const mesh: THREE.Mesh = ref.current;

	// 		camera.lookAt(mesh.position);
	// 		camera.position.set(
	// 			mesh.position.x - 1,
	// 			mesh.position.y - 0.5,
	// 			mesh.position.z + 5,
	// 		);
	// 	}
	// });

	return (
		<mesh
			ref={ref}
			visible={true}
			castShadow={true}
			position={position}
			receiveShadow={true}
		>
			<icosahedronGeometry args={[1, 0]} />
		</mesh>
	);
};

export default D20;
