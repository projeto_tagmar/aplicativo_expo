import * as CANNON from "cannon-es";
import THREE, { TypedArray } from "three";
import { getColumnColors } from "../../util/constants";
import { ObjectConfig } from "../materials";

export const getVertices = (position: TypedArray): CANNON.Vec3[] => {
	const out: CANNON.Vec3[] = [];
	for (let i = 0; i < position.length; i += 3) {
		out.push(new CANNON.Vec3(position[i], position[i + 1], position[i + 2]));
	}
	return out;
};

export const getFaces = (position: TypedArray): number[][] => {
	const out: number[][] = [];
	for (let i = 0; i < position.length / 3; i += 3) {
		out.push([i, i + 1, i + 2]);
	}
	return out;
};

// https://jsfiddle.net/5cm6wvqz/1/
export const setFaceColors = (
	mesh: THREE.Mesh,
	coluna: number,
	config: ObjectConfig,
): void => {
	const columnColors = getColumnColors(coluna);

	const piece = mesh.geometry.toNonIndexed();
	const material = new THREE.MeshPhongMaterial({
		vertexColors: true,
		map: config.texture,
		emissive: config.emissive,
	});
	const positionAttribute = piece.getAttribute("position");
	const colors: number[] = [];

	const color = new THREE.Color();

	let counter = 0;
	for (let i = 0; i < positionAttribute.count; i += 3) {
		const faceColor = columnColors[counter];

		color.setHex(faceColor);

		colors.push(color.r, color.g, color.b);
		colors.push(color.r, color.g, color.b);
		colors.push(color.r, color.g, color.b);

		counter++;
	}

	// define the new attribute
	piece.setAttribute("color", new THREE.Float32BufferAttribute(colors, 3));
	mesh.geometry = piece;
	mesh.material = material;
};

export const changeToColor = (mesh: THREE.Mesh, color: number): void => {
	const mat = mesh.material;
	const matList = Array.isArray(mat) ? mat : [mat];
	// biome-ignore lint/suspicious/noExplicitAny: <explanation>
	const m = matList[0].clone() as any;
	m.color.setHex(color);
	m.vertexColors = false;
	m.needsUpdate = true;
	mesh.material = m;
};
