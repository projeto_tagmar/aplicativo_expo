import * as CANNON from "cannon-es";
import ExpoTHREE from "expo-three";
import { Textures, getTexture } from "../assets/textures";

export type ObjectConfig = {
	material: CANNON.Material;
	texture: ExpoTHREE.THREE.Texture;
	color: number;
	emissive?: number;
};

enum Materials {
	WOOD = 0,
	PLASTIC = 1,
	VELVET = 2,
	METAL = 3,
}

const WOOD: ObjectConfig = {
	material: new CANNON.Material({
		friction: 0.1,
		restitution: 0.9,
	}),
	texture: getTexture(Textures.WOOD),
	color: 0x0205aa,
};

const PLASTIC: ObjectConfig = {
	material: new CANNON.Material({
		friction: 1,
		restitution: 2,
	}),
	texture: getTexture(Textures.PAPER),
	color: 0x606060,
};

const VELVET: ObjectConfig = {
	material: new CANNON.Material({
		friction: 0.3,
		restitution: 0.2,
	}),
	texture: getTexture(Textures.VELVET),
	color: 0x0f6621,
	emissive: 0x011605,
};

const METAL: ObjectConfig = {
	material: new CANNON.Material({
		friction: 0.2,
		restitution: 0.2,
	}),
	texture: getTexture(Textures.METAL),
	color: 0x909090,
};

const getObjectProps = (material: Materials): ObjectConfig => {
	switch (material) {
		case Materials.PLASTIC:
			return PLASTIC;

		case Materials.WOOD:
			return WOOD;
		case Materials.VELVET:
			return VELVET;
		case Materials.METAL:
			return METAL;
	}
};

export { Materials, getObjectProps };
