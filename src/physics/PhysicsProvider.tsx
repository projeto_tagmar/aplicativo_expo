import { useFrame } from "@react-three/fiber";
import * as CANNON from "cannon-es";
import React, {
	PropsWithChildren,
	createContext,
	useContext,
	useEffect,
	useRef,
	useState,
} from "react";
import THREE from "three";
const WorldContext = createContext<CANNON.World>({} as unknown as CANNON.World);

// https://github.com/pmndrs/use-cannon/issues/63

export const useCannonWorld = (): CANNON.World => {
	return useContext(WorldContext);
};

export const PhysicsProvider = ({ children }: PropsWithChildren) => {
	const [world] = useState(() => new CANNON.World());

	useEffect(() => {
		world.gravity.set(0, 0, -9.82);
		world.broadphase = new CANNON.NaiveBroadphase();
		(world.solver as CANNON.GSSolver).iterations = 14;
		world.allowSleep = true;
		world.defaultContactMaterial.restitution = 0.2;
	}, [world]);

	// Run world stepper every frame
	useFrame(() => world.step(1 / 30));

	return (
		<WorldContext.Provider value={world}>{children}</WorldContext.Provider>
	);
};

export const useCannon = (
	{ ...props },
	fn: (b: CANNON.Body) => void,
	deps = [],
) => {
	// biome-ignore lint/suspicious/noExplicitAny: <explanation>
	const ref = useRef<any>();
	const world = useCannonWorld();
	const [body] = useState(() => new CANNON.Body(props));

	// biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
	useEffect(() => {
		// Call function so the user can add shapes, positions, etc. to the body
		fn(body);
		world.addBody(body);
		return () => world.removeBody(body);
	}, deps);

	useFrame(() => {
		if (ref.current) {
			// Transport cannon physics into the referenced threejs object
			const { position, quaternion } = body;
			const { x: px, y: py, z: pz } = position;
			const { x: qx, y: qy, z: qz, w: qw } = quaternion;
			ref.current.position.copy(new THREE.Vector3(px, py, pz));
			ref.current.quaternion.copy(new THREE.Quaternion(qx, qy, qz, qw));
		}
	});

	return ref;
};
