import * as CANNON from "cannon-es";
import React from "react";
import { useCannon } from "../PhysicsProvider";
import { Materials, getObjectProps } from "../materials";

const PoolTable = () => {
	const props = getObjectProps(Materials.VELVET);

	const ref = useCannon({ mass: 0 }, (body) => {
		body.addShape(new CANNON.Plane());
		body.position.set(0, 0, 0);
		body.type = CANNON.Body.STATIC;

		body.material = props.material;
	});
	return (
		<mesh
			ref={ref}
			name={"mesa"}
			scale={[1, 1, 1]}
			receiveShadow={true}
			rotation={[-90, 0, 0]}
		>
			<planeGeometry args={[20, 20]} />

			<meshPhongMaterial
				fog={true}
				dithering={true}
				map={props.texture}
				color={props.color}
				flatShading={false}
				lightMapIntensity={0.2}
				emissive={props.emissive}
			/>
		</mesh>
	);
};

export default PoolTable;
