import React from "react";

const Lights = () => {
	return (
		<group name="lights">
			{/* <ambientLight args={[0xddd5bc]} intensity={0.001} /> */}
			<directionalLight
				args={[0xefda9b, 10]}
				castShadow
				position={[0, 10, 10]}
			/>
			<hemisphereLight args={["#bbeaea", "#e8eab4", 2]} />
		</group>
	);
};

export default Lights;
