import React from "react";

const Ambient = () => {
	return (
		<group name="ambient">
			<color attach="background" args={["#202020"]} />
		</group>
	);
};

export default Ambient;
