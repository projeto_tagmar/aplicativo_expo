# aplicativo_expo

## Política de privacidade

Não coletamos seus dados.

### 📝 Notas

- [Expo Router: Docs](https://expo.github.io/router)
- [Three](https://www.youtube.com/watch?v=xD5ER-4MRkc)
- [Cannon](https://github.com/pmndrs/use-cannon/issues/63)
- [Dice](https://tympanus.net/codrops/2023/01/25/crafting-a-dice-roller-with-three-js-and-cannon-es/)
